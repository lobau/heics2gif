# heics2gif

FFmpeg WASM requires 2 headers:
```
Cross-Origin-Embedder-Policy:require-corp
Cross-Origin-Opener-Policy:same-origin
```

You can serve the app with the correct headers using the provided python script

```sh
python server.py
```