from http.server import HTTPServer, SimpleHTTPRequestHandler
import ssl

class CORSHTTPRequestHandler(SimpleHTTPRequestHandler):
    def end_headers(self):
        self.send_header('Cross-Origin-Embedder-Policy', 'require-corp')
        self.send_header('Cross-Origin-Opener-Policy', 'same-origin')
        super().end_headers()

def run(server_class=HTTPServer, handler_class=CORSHTTPRequestHandler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    print("Serving at port 8000")
    httpd.serve_forever()

if __name__ == '__main__':
    run()