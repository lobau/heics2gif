#!/bin/bash

file=$1
folder="${file}_temp"
output="${file}.gif"

# Create a temporary folder
mkdir -p $folder

# Extract the main image and mask
ffmpeg -i $file -map 0:0 "${folder}/main_%04d.png"
ffmpeg -i $file -map 0:1 "${folder}/mask_%04d.png"

# Initialize frame counter
frame_counter=1

# Combine the main image and mask, then create a GIF
for i in ${folder}/main_*.png; do
    main_frame="${folder}/main_$(printf "%04d" $frame_counter).png"
    mask_frame="${folder}/mask_$(printf "%04d" $frame_counter).png"
    merged_frame="${folder}/merged_$(printf "%04d" $frame_counter).png"

    # Check if the main and mask files exist
    if [ ! -f "$main_frame" ] || [ ! -f "$mask_frame" ]; then
        echo "Missing frame: $main_frame or $mask_frame"
        continue
    fi

    ffmpeg -i "$main_frame" -i "$mask_frame" -filter_complex "[1:v]format=gray,geq=lum='p(X,Y)':a='p(X,Y)'[mask];[0:v][mask]alphamerge" "$merged_frame"
    
    ((frame_counter++))
done


ffmpeg -i "${folder}/merged_%04d.png" -vf palettegen=reserve_transparent=1 "${folder}/palette.png"

# Create GIF
ffmpeg -framerate 15 -i "${folder}/merged_%04d.png" -i "${folder}/palette.png" -lavfi paletteuse=alpha_threshold=128 -gifflags -offsetting $output

# ffmpeg -i "${folder}/merged_%04d.png" -y -f gif $output

# Clean up the temporary files (optional)
# rm -rf $folder
